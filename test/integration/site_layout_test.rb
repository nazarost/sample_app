require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  def setup 
    @user = users(:nazar)
  end

  test "layout_links"  do
  	get root_path
  	assert_template 'static_pages/home'
  	assert_select "a[href=?]", root_path, count: 2
  	assert_select "a[href=?]", help_path
  	assert_select "a[href=?]", about_path
  	assert_select "a[href=?]", contact_path
  	get contact_path
  	assert_select "title", full_title("Contact")
    get signup_path
    assert_select "title", full_title("Sign up")
  end

  test "all layout_links" do
    #test links when not logged in
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    #log in
    log_in_as(@user)
    get root_path
    assert_template 'static_pages/home'
    #test links when logged in
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", login_path, count:0  # link disappeared
    assert_select "a[href=?]", users_path

    assert_select "a[href=?]", user_path(@user)     # Profile
    assert_select "a[href=?]", edit_user_path(@user)# Settings
    assert_select "a[href=?]", logout_path

    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path

    #test if logout link disappeares if logout done
    #log_out_as(@user)
    #assert_select "a[href=?]", logout_path, count:0

  end
end
